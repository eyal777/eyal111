import java.util.Iterator;
import java.util.Vector;

public class rec2 {

    public static String ZeroPath(int[][] arr){
        String path = "{";
        boolean flag = true;
        for(int i = 0 ;i < arr.length ;i++)
            for(int j = 0 ;j < arr[0].length ;j++) {
                if (arr[i][j] == 0){
                    path += "(" + i + "," + j + ") -> ";
                }else{

                }
            }
        path += "}";
        return path;
    }

    public static int getSizeNeighbors(int[][] mat ,int i ,int j){
        int r = mat.length;
        int c = mat[0].length;
        if(i == 0 && j == 0 || i == 0 && j == c - 1 || i == r - 1 && j == 0 || i == r - 1 && j == c - 1)
            return 3;
        if(i == 0 && (j > 0 && j < c - 1) || i == r - 1 && (j > 0 && j < c - 1) || (i > 0 && i < r - 1) && j == 0 || (i > 0 && i < r - 1) && j == c - 1)
            return 5;
        return 8;
    }

    public static String[] getNeighbors(int[][] mat ,int r ,int c){
        String[] neighbors = null;
        int k = 0;
        for(int i = r - 1 ;i <= r + 1 && i >= 0 && i < mat.length ;i++)
            for(int j = c - 1 ;j <= c + 1 && j >= 0 && j < mat[0].length ;j++)
                if(i != r || j != c)
                    neighbors[k++] = "(" + i + "," + j + ")";
        return neighbors;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static boolean isPrime(int n){
        for(int i = 2 ;i <= Math.sqrt(n) ;i++)
            if(n % i == 0)
                return false;
        return true;
    }

    public static Vector<Integer> primaryFactors(int n){
        Vector<Integer> v = new Vector<Integer>();
        int i = 2;
        while(!isPrime(n)){
            if(n % i == 0) {
                v.add(i);
                n = n / i;
            }else
                i++;
        }
        v.add(n);
        return v;
    }

    public static int getClosePrime(int n){
        int next = n ,prev = n;
        while(next <= 2*n || prev > 1){
            if(isPrime(next)) return next;
            else next ++;
            if(isPrime(prev)) return prev;
            else prev --;
        }
        return n;
    }

    public static int getClosePrimeDirect(int n){
        int d = Math.min(n - getPrevPrimeRec(n),getNextPrimeRec(n) - n);
        if(d == n - getPrevPrimeRec(n))
            return getPrevPrimeRec(n);
        return getNextPrimeRec(n);
    }

    public static int getNextPrimeRec(int p){
        if(isPrime(p))
            return p;
        else
            return getNextPrimeRec(p + 1);
    }

    public static int getPrevPrimeRec(int p){
        if(isPrime(p))
            return p;
        else
            return getPrevPrimeRec(p - 1);
    }

    public static int gcd(int a ,int b){
        int t = Math.min(a,b);
        if(a >= b){
            int r = a % b;
            while(r != 0){
                t = b;
                r = a % b;
                a = b;
                b = r;
            }
        }else{
            int r = b % a;
            while(r != 0){
                t = a;
                r = b % a;
                b = a;
                a = r;
            }
        }
        return t;
    }

    public static int gcdRec(int a ,int b){
        if(a % b == 0)
            return b;
        else
            return gcdRec(b,a % b);
    }

    public static boolean sharpInitial(int n){
        Vector<Integer> v = primaryFactors(n);
        boolean flag = true;
        for(int i = 0 ;i < v.size() - 1;i++) {
            if(v.get(i).equals(v.get(i + 1)))
                flag = true;
            else{
                flag = false;
                break;
            }
        }
        return flag;
    }

    public static int getSumPrimes(int n){
        int sum = 0;
        for(int i = 2 ;i <= n ;i++)
            if(isPrime(i))
                sum += i;
        return sum;
    }

    public static int getSumPrimesRec(int n){
        if(n % 2 == 0)
            return 2;
        if(!isPrime(n))
            return getSumPrimesRec(getPrevPrimeRec(n));
        return n + getSumPrimesRec(getPrevPrimeRec(n));
    }

    public static void main(String[] args) {
       int k = getSumPrimesRec(63);
       System.out.print(k);
    }
}
