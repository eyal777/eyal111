import java.util.Arrays;

public class rec {

    public static boolean anagram1(String s1, String s2) {
        char[] arr1 = new char[26];
        char[] arr2 = new char[26];
        for (char i = 'a'; i <= 'z'; i++) {
            arr1[i - 'a'] = 0;
            arr2[i - 'a'] = 0;
        }
        for (int i = 0; i < s1.length(); i++)
            arr1[s1.charAt(i) - 'a']++;
        for (int i = 0; i < s2.length(); i++)
            arr2[s2.charAt(i) - 'a']++;
        boolean flag = true;
        for (int i = 0; i < 26; i++) {
            if (arr1[i] == arr2[i])
                flag = true;
            else {
                flag = false;
                break;
            }
        }
        return flag;
    }

    public static boolean anagram2(String s1, String s2) {
        boolean flag = true;
        for (int i = 0; i < s1.length(); i++) {
            for (int j = 0; j < s2.length(); j++) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    flag = true;
                    s2 = s2.replace("" + s2.charAt(j) + "", "");
                    break;
                } else
                    flag = false;
            }
            if (!flag)
                break;
        }
        return flag;
    }

    public static int[] Selection_Sort(int[] arr){
        for(int i = 0; i < arr.length - 1; i++){
            int index = i;
            for(int j = i + 1; j < arr.length; j++)
                if (arr[j] < arr[index])
                    index = j; //searching for lowest index
            int smallerNumber = arr[index];
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        return arr;
    }

    public static int[] Bubble_Sort(int[] arr){
        int n = arr.length;
        int temp = 0;
        for(int i = 0; i < n; i++){
            for(int j = 1; j < (n - i); j++){
                if(arr[j - 1] > arr[j]){
                    //swap elements
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    public static int[] Insertion_Sort(int arr[]){
        int n = arr.length;
        for (int j = 1; j < n; j++){
            int key = arr[j];
            int i = j - 1;
            while((i > -1) && (arr[i] > key)){
                arr[i + 1] = arr[i];
                i--;
            }
            arr[i + 1] = key;
        }
        return arr;
    }

    /////////////////////////////////////////////////////////

    public static int HighestValue(int[] arr){
        int max = Integer.MIN_VALUE;
        for(int i = 0 ;i < arr.length ;i++)
            if(max < arr[i])
                max = arr[i];
        return max;
    }

    public static int SmallestValue(int[] arr){
        int min = Integer.MAX_VALUE;
        for(int i = 0 ;i < arr.length ;i++)
            if(min > arr[i])
                min = arr[i];
        return min;
    }

    public static int[] SortSection(int[] arrToSort ,int from ,int to) {
        int[] section = Arrays.copyOfRange(arrToSort ,from ,to);
        int[] sortedSection = CountingSort(section);
        int[] output = Arrays.copyOf(arrToSort ,arrToSort.length);
        for (int i = from ;i < to ;i++)
            output[i] = sortedSection[i - from];
        return output;
    }

    public static int[] SortByCountingGroups(int[] arr) {
        int factor = Math.abs(SmallestValue(arr)) < 0 ? Math.abs(SmallestValue(arr)) : 0;
        int maxNumber = HighestValue(arr);
        boolean[] buckets = new boolean[maxNumber + factor + 1];
        int[] output = Arrays.copyOf(arr, arr.length);
        int prevTo = 0;
        for (int i = 0 ;i < arr.length ;i++) {
            int curIndex = arr[i] + factor;
            if (buckets[curIndex]) {
                output = SortSection(output ,prevTo ,i);
                prevTo = i;
                Arrays.fill(buckets ,false);
            }else
                buckets[curIndex] = true;
        }
        return output;
    }


    public static int[] CountingSort(int[] arr) {
        int factor = Math.abs(SmallestValue(arr)) < 0 ? Math.abs(SmallestValue(arr)) : 0;
        int maxNumber = HighestValue(arr);
        int[] buckets = new int[maxNumber + factor + 1];
        for (int i : arr)
            buckets[i + factor]++;
        int[] output = new int[arr.length];
        int index = 0;
        for (int i = 0; i < buckets.length; i++)
            for(int j = 0; j < buckets[i]; j++)
                output[index++] = i - factor;
        return output;
    }

    public static void main(String[] args) {
        int[] arr = {2, 7, 3, 1, 6, 5, 6, 7, 3, 2, 8, 7, 9, 9, 3, 0, 4, 1, 3, 0, 4, 4};
        arr = SortByCountingGroups(arr);
        for (int i = 0; i < arr.length; i++)
            System.out.print(arr[i] + " ");
        // {1,2,3,5,6,7,2,3,6,7,8,7,9,0,1,3,4,9,0,3,4,4} - if exist return of certain value so init set of countingSort's values
        // {0,0,2,2,4,4,4,6,6,8,1,1,3,3,3,3,5,7,7,7,9,9} - Sort Even First
        // {1,1,3,3,3,3,5,7,7,7,9,9,0,0,2,2,4,4,4,6,6,8} - Sort Odd First
    }
}
